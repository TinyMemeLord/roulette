import java.util.Scanner;

public class Roulette {
    
    public static void main(String args[]) {

        RouletteWheel rw = new RouletteWheel();
        Scanner sc = new Scanner(System.in);
        int bet = 0;
        System.out.println("Please insert the total cash amount:");
        int totalMoney = sc.nextInt();
        System.out.println("Whould you like to make a bet?");
        String makeBet = sc.next();
        if (makeBet.equals("y")) {

            System.out.println("What number would you like to bet on? 1-36");
            int betNumber = sc.nextInt();
                do {
                    System.out.println("\nInsert a bet amount:");
                    bet = sc.nextInt();
                    
                    if (bet > totalMoney) {

                        System.out.println("You do not have enough money to make that bet. Your total amount is: " + totalMoney);
                    }
                }
                while (bet > totalMoney);
        }
    }
}
